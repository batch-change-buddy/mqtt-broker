# MQTT Broker: 2.0 had some trouble https://github.com/eclipse/mosquitto/issues/2040
FROM eclipse-mosquitto:2.0.15
COPY config/mosquitto.conf ./mosquitto/config/